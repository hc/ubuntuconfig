#!/bin/sh

# Used to handle app:// protocol to open apps from browser
# see http://askubuntu.com/questions/330937/is-it-possible-to-open-an-ubuntu-app-from-html

logger Handling $@
app=$(echo $@ | sed -r 's/app:\/\///g')
nohup $app > /dev/null 2>&1 &

exit
