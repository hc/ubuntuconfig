#!/bin/sh

PROGNAME=`basename $0`
BRANCH=$1
SVN_CMD=`which svn2`

die() {
	echo "$PROGNAME: $*"
	exit 1
}

if [ "x$BRANCH" = "x" ]; then 
	die "Please specify a branch."
fi
	
if [ "x$SVN_CMD" = "x"]; then
	die "SVN command not found." 
fi
