#!/bin/sh

# Used to handle app:// protocol to open apps from browser
# see http://askubuntu.com/questions/330937/is-it-possible-to-open-an-ubuntu-app-from-html

logger "Handling appt for $@"
app=$(echo $@ | sed -r 's/app:\/\///g')
/usr/bin/gnome-terminal -x $app #> /dev/null 2>&1 &

exit
