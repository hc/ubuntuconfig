#!/usr/bin/python

import argparse
import os
import subprocess

CHECKOUT_DIR_PREFIX = "Fx"

REPO_BASE_URL = "http://fxrbg3sw01.fnb.co.za/svn/fnbforex/Services/"
REPO_TEMPLATES = [
        "FxVantage/branches/FxVantage %s",
        "ProcessEngine/branches/retail %s",
        "RetailForex/branches/release-v%s"
        ]

def main():
    branch = parseCommandArguments().get('branch')
    dirname = createDirectoryForBranch(branch)
    checkout(branch, dirname)

def parseCommandArguments():
    parser = argparse.ArgumentParser(description='Checkout Vantage')
    parser.add_argument('-b', '--branch', required=True,
            help='the branch version to check out')
    return vars(parser.parse_args())

def createDirectoryForBranch(branch):
    dirname = CHECKOUT_DIR_PREFIX + branch
    os.makedirs(dirname)
    return dirname

def checkout(branch, dirname):
    os.chdir(dirname)
    for template in REPO_TEMPLATES:
        repo_url = REPO_BASE_URL + template % branch
        svnCheckout(repo_url)

def svnCheckout(repo_url):
        subprocess.call(['svn', 'co', repo_url])

if __name__ == '__main__':
	main()
