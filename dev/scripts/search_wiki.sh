#!/bin/zsh

QUERY=$(matedialog --title="Search local Wiki" --entry --text="Your search query")
if [ "x" != "x$QUERY" ]; then
	 i3-msg "[class="Chromium"] focus" >/dev/null
         chromium-browser --user-data-dir="~/.config/wiki" "http://localhost:9090/_search?patterns=$QUERY"
    fi

