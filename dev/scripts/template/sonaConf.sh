#!/bin/sh

echo Create Sona Project configuration file sonar-project.properties
echo See: http://docs.codehaus.org/display/SONAR/Analyzing+with+SonarQube+Runner
echo
read -p "Project Key: " KEY
read -p "Project Name: " NAME
read -p "Project Version: " VERSION
echo
cat <<EOF
# Generated with ~/dev/scripts/template/sonaConf.sh
# hcvst@izazi.com

# Required metadata
sonar.projectKey=fnb:$KEY
sonar.projectName=$NAME
sonar.projectVersion=$VERSION
 
# Path to the parent source code directory.
# Path is relative to the sonar-project.properties file. Replace "\" by "/" on Windows.
# Since SonarQube 4.2, this property is optional if sonar.modules is set. 
# If not set, SonarQube starts looking for source code from the directory containing 
# the sonar-project.properties file.
sonar.sources=src
 
# Encoding of the source code
sonar.sourceEncoding=UTF-8
 
# Additional parameters
sonar.my.property=value
EOF 
