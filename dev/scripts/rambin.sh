#!/bin/bash

LOC=/tmp/ramdisk

SRCS=(
/opt/java/jdk1.6.0_37 
/opt/springsource/sts-3.5.1.RELEASE 
/opt/idea/idea-IU-135.909 
) 

echo $SRCS

echo Creating RAM Disk
mkdir -p $LOC

sudo mount -t ramfs ramfs $LOC
sudo chown hcvst:hcvst $LOC

function prompt(){
    echo -n "Copy $1 to $LOC? "
    read RETVAL
    if [[ RETVAL =~ ^[Yy] ]]; then
        return 0
    else
        return 1
    fi 
}

function copy(){
    echo Copying $1 to $LOC
    cp -r $1 $LOC
    echo Updating path
    export PATH=$1:$PATH
}

function addToPath(){
    P="${LOC}/$(basename $1)"
    echo Prefixing PATH with $P
    export PATH=$P:$PATH
}

function promptToCopy(){
    if prompt $1; then
        copy $1 
	addToPath $1
    else
        echo Skipping $1
    fi
}


for src in $SRCS; do
	promptToCopy $src
done

echo Run 'IDEA_JDK=/tmp/ramdisk/jdk1.6.0_37 idea.sh' 
