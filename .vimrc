execute pathogen#infect()
syntax on
filetype plugin indent on
let g:go_disable_autoinstall = 1

nmap <F8> :TagbarToggle<CR>



let g:tagbar_type_go = {
    \ 'ctagstype' : 'go',
    \ 'kinds'     : [
        \ 'p:package',
        \ 'i:imports:1',
        \ 'c:constants',
        \ 'v:variables',
        \ 't:types',
        \ 'n:interfaces',
        \ 'w:fields',
        \ 'e:embedded',
        \ 'm:methods',
        \ 'r:constructor',
        \ 'f:functions'
    \ ],
    \ 'sro' : '.',
    \ 'kind2scope' : {
        \ 't' : 'ctype',
        \ 'n' : 'ntype'
    \ },
    \ 'scope2kind' : {
        \ 'ctype' : 't',
        \ 'ntype' : 'n'
    \ },
    \ 'ctagsbin'  : 'gotags',
    \ 'ctagsargs' : '-sort -silent'
    \ }


colorscheme molokai

" --------------------------------------------
" http://learnvimscriptthehardway.stevelosh.com
" --------------------------------------------

set number
iabbrev @@ hc@vst.io
inoremap <c-d> <esc>ddi 

" disable arrow keys
inoremap <Up> <nop>
inoremap <Down> <nop>
inoremap <Left> <nop>
inoremap <Right> <nop>

nnoremap <Up> <nop>
nnoremap <Down> <nop>
nnoremap <Left> <nop>
nnoremap <Right> <nop>

set hlsearch incsearch

" --------------------------------------------
" powerline fonts for airline plugin
" https://github.com/Lokaltog/powerline-fonts
" --------------------------------------------

let g:airline_powerline_fonts = 1
let g:airline_theme='powerlineish'
set laststatus=2

" markdown
au BufRead,BufNewFile *.page set filetype=markdown

" hightlight /etc/hosts
au BufRead /etc/hosts :match Include /^[^#].*/
au BufRead /etc/hosts :1match Question /http\S*/ 
au BufRead /etc/hosts :2match Include /^[0-9.]\{1,\}/
au BufRead /etc/hosts :3match ErrorMsg /127.0.0.1/ 

" transparent background
hi Normal guibg=NONE ctermbg=NONE

" enable paste mode 
set paste

" undo
set undodir=~/.vim/undodir
set undofile
"maximum number of changes that can be undone
set undolevels=1000 
"maximum number lines to save for undo on a buffer reload
set undoreload=10000 
