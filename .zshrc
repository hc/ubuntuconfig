#set -x
# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
#ZSH_THEME="robbyrussell"
#ZSH_THEME="agnoster"
#ZSH_THEME="random"
ZSH_THEME="steeef"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Uncomment this to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how often before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want to disable command autocorrection
# DISABLE_CORRECTION="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Uncomment following line if you want to disable marking untracked files under
# VCS as dirty. This makes repository status check for large repositories much,
# much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment following line if you want to  shown in the command execution time stamp 
# in the history command output. The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|
# yyyy-mm-dd
# HIST_STAMPS="mm/dd/yyyy"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git svn mvn colored-man jira debian docker colorize autojump catimg common-aliases celery gulp ssh-agent)

source $ZSH/oh-my-zsh.sh

# User configuration

export PATH=$HOME/bin:/usr/local/bin:$PATH
# export MANPATH="/usr/local/man:$MANPATH"

# # Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# hcvst
setxkbmap -option ctrl:nocaps # disable capslock
#fortune de | cowsay | toilet -f term -F metal
WIKI_MOTD=$(curl http://localhost:9090/_showraw/MOTD 2> /dev/null)
FORTUNE=$(fortune de) 
printf "$FORTUNE \n-------------------------------------\n $WIKI_MOTD" | cowsay | lolcat
#cat ~/dev/scripts/weather/weather.dat


function wikipedia(){
     dig +short txt ${1}.wp.dg.cx
}
 
function startjboss(){
     /home/hcvst/local/efx-jboss/opt/jboss/jboss-6.1.0.Final/bin/run.sh | multitail -m 0 -CS log4j -j
}

# hcvst
export COMMAND_NOT_FOUND_INSTALL_PROMPT=true
export PATH=/opt/java/current/bin:$PATH
export JAVA_HOME=/opt/java/current
export MAVEN_OPTS='-Xms256m -XX:MaxPermSize=1024m -Xmx1024m'
export TERM=xterm-256color
export PATH=$PATH:/home/hcvst/dev/android-sdk-linux/platform-tools:/home/hcvst/dev/android-sdk-linux/tools
export PATH=$PATH:/home/hcvst/npm/bin
. /etc/zsh_command_not_found

export GOROOT=/opt/go/current
export PATH=$PATH:$GOROOT/bin

export GOPATH=$HOME/dev/go
export PATH=$PATH:$GOPATH/bin
export PATH=$PATH:/opt/go_appengine
export PATH=$HOME/.cabal/bin:$PATH
export PATH=/home/hcvst/bin/apache-maven-3.0.4/bin:$PATH

# https://gts-it.fnb.co.za/wiki/display/BBG/BBG+Installing+JBoss+6
export JBOSS_HOME=/opt/jboss/current
# https://gts-it.fnb.co.za/wiki/display/BBG/Installing+Oracle+XE
export ORACLE_HOME=/u01/app/oracle/product/11.2.0/xe
export ORACLE_SID=XE
#export NLS_LANG=`$ORACLE_HOME/bin/nls_lang.sh`
export ORACLE_BASE=/u01/app/oracle
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:$LD_LIBRARY_PATH
export PATH=$ORACLE_HOME/bin:$PATH


alias -g N="; notify-send -t 2000 DONE $(echo $history[$HISTCMD] | awk '{print $1}')"
alias -g Ai="sudo tsocks apt-get install "
alias -g afl="apt-file list "
alias -g LOL="|lolcat"

#hcvst allow bash style comments
setopt interactivecomments 

# custom zle widget
function _horizontal_ruler(){
    echo "$(tput setab 1) >>> $(date) <<< "
    zle .accept-line
}


zle -N _horizontal_ruler
bindkey '\eh' _horizontal_ruler

# search jars
function searchjars(){
     find . -name "*jar" | xargs -n 1 sh -c \
        'unzip -l $0 | sed "s|^|$(tput setaf 4)$0$(tput setaf 1)|"' |\
      	awk '{print $1,$5}' |\
       	grep $1 
}

function cmvn(){
    mvn "$@" | multitail -cS maven -J    
}

function kmvn(){
    mvn "$@" | sed \
	    -e "s/\(\[INFO\]\)/$(tput setaf 4)\1$(tput sgr 0)/" \
	    -e "s/\(\[SUCCESS\]\)/$(tput setaf 2)\1$(tput sgr 0)/" \
	    -e "s/\(BUILD SUCCESS\)/$(tput setaf 2)$(tput bold)\1$(tput sgr 0)/" \
	    -e "s/\(\[WARNING.*\)/$(tput setaf 3)\1$(tput sgr 0)/" \
	    -e "s/\(\[ERROR\]\)\(.*\)/$(tput setab 1)\1$(tput sgr 0)$(tput setaf 1)\2$(tput sgr 0)/" \
	    -e "s/\(--- .* ---\)/$(tput setaf 6)$(tput smul)\1$(tput sgr 0)/" \
	    -e "s/\(-\{4,\}\)/$(tput rev)$(tput dim)\1$(tput sgr 0)/"
}


function wiki_search(){
    i3-msg "[class="Chromium"] focus" >/dev/null
    chromium-browser "http://localhost:9090/_search?patterns=$@"
    }

alias ws="wiki_search"

# disable touchpad
xinput --disable $(xinput --list | grep Touch| sed 's/.*id=\(..\).*/\1/')

export PATH=$PATH:/opt/android-studio/bin
export ANDROID_HOME=/home/hcvst/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
export PATH=$PATH:/home/hcvst/dev/scripts

export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
export PATH=/home/hcvst/bin/anaconda3/bin:$PATH


# Backbase
export M2_HOME=/usr/share/maven
export M2=${M2_HOME}/bin


# The next line updates PATH for the Google Cloud SDK.
if [ -f '/opt/google-cloud-sdk/path.zsh.inc' ]; then source '/opt/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/opt/google-cloud-sdk/completion.zsh.inc' ]; then source '/opt/google-cloud-sdk/completion.zsh.inc'; fi

#nvm 
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm


#kubectl (plugin should have worked but did not)
if [ $commands[kubectl] ]; then
  source <(kubectl completion zsh)
fi

